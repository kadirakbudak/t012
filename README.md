
## Installation

### Install SLATE using CMake
Leconte

```
cmake .. -DCMAKE_INSTALL_PREFIX=./install -DCMAKE_CUDA_ARCHITECTURES=70
```

### Install slate-tutorial

These are required if makefile is used to compile slate:
```
export LD_LIBRARY_PATH=${HOME}/t012/slate-dev/lapackpp/install/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${HOME}/t012/slate-dev/blaspp/install/lib:$LD_LIBRARY_PATH
```

`make.inc` in slate-tutorial:

```
#slate_dir = ${HOME}/t012/cmake-capi/build/install
slate_dir = ${HOME}/t012/slate-dev/install
scalapack_libs = -L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64
```

## Progress

### 2022-02-08 Tuesday

Trying to install slate using cmake

### 2022-02-09 Wednesday

Installed slate using cmake. Defined -DCMAKE_CUDA_ARCHITECTURES=70 for leconte

```
cmake .. -DCMAKE_INSTALL_PREFIX=./install -DCMAKE_CUDA_ARCHITECTURES=70 -Dc_api=1
```

```
python tools/c_api/generate_util.py include/slate/c_api/types.h include/slate/c_api/util.hh
python tools/c_api/generate_matrix.py  include/slate/Tile.hh include/slate/c_api/matrix.h
python tools/c_api/generate_wrappers.py  src/c_api/wrappers.cc include/slate/c_api/wrappers.h
```

```
kadir@leconte 09:34:14 Feb 09 Wed ~/t012/slate-dev $ echo " " >> include/slate/c_api/types.h
kadir@leconte 09:34:25 Feb 09 Wed ~/t012/slate-dev $ make lib  VERBOSE=1
python tools/c_api/generate_util.py include/slate/c_api/types.h include/slate/c_api/util.hh
mpicxx -g -Werror -DSLATE_NO_HIP -O3 -std=c++17 -Wall -Wshadow -pedantic -MMD -fPIC -fopenmp -DSLATE_WITH_MKL -DSLATE_NO_HIP -I./blaspp/include -I./lapackpp/include -I./include -I./src -c src/c_api/util.cc -o src/c_api/util.o
mpicxx -g -Werror -DSLATE_NO_HIP -O3 -std=c++17 -Wall -Wshadow -pedantic -MMD -fPIC -fopenmp -DSLATE_WITH_MKL -DSLATE_NO_HIP -I./blaspp/include -I./lapackpp/include -I./include -I./src -c src/c_api/matrix.cc -o src/c_api/matrix.o
mpicxx -g -Werror -DSLATE_NO_HIP -O3 -std=c++17 -Wall -Wshadow -pedantic -MMD -fPIC -fopenmp -DSLATE_WITH_MKL -DSLATE_NO_HIP -I./blaspp/include -I./lapackpp/include -I./include -I./src -c src/c_api/wrappers.cc -o src/c_api/wrappers.o
mpicxx -g -Werror -DSLATE_NO_HIP -O3 -std=c++17 -Wall -Wshadow -pedantic -MMD -fPIC -fopenmp -DSLATE_WITH_MKL -DSLATE_NO_HIP -I./blaspp/include -I./lapackpp/include -I./include -I./src -c src/c_api/wrappers_precisions.cc -o src/c_api/wrappers_precisions.o
mkdir -p lib
mpicxx -fPIC -fopenmp -L./blaspp/lib -L./lapackpp/lib \
        src/auxiliary/Debug.o src/auxiliary/Trace.o src/core/Memory.o src/version.o src/work/work_trsm.o src/work/work_trmm.o src/internal/internal_comm.o src/internal/internal_copyhb2st.o src/internal/internal_copytb2bd.o src/internal/internal_gecopy.o src/internal/internal_gbnorm.o src/internal/internal_geadd.o src/internal/internal_gemm.o src/internal/internal_gemmA.o src/internal/internal_genorm.o src/internal/internal_gebr.o src/internal/internal_geqrf.o src/internal/internal_geset.o src/internal/internal_getrf.o src/internal/internal_getrf_nopiv.o src/internal/internal_hebr.o src/internal/internal_hemm.o src/internal/internal_hemmA.o src/internal/internal_hbnorm.o src/internal/internal_henorm.o src/internal/internal_her2k.o src/internal/internal_herk.o src/internal/internal_hettmqr.o src/internal/internal_potrf.o src/internal/internal_swap.o src/internal/internal_symm.o src/internal/internal_synorm.o src/internal/internal_syr2k.o src/internal/internal_syrk.o src/internal/internal_transpose.o src/internal/internal_trmm.o src/internal/internal_trnorm.o src/internal/internal_trsm.o src/internal/internal_trtri.o src/internal/internal_trtrm.o src/internal/internal_ttmqr.o src/internal/internal_ttmlq.o src/internal/internal_ttqrt.o src/internal/internal_ttlqt.o src/internal/internal_tzcopy.o src/internal/internal_tzset.o src/internal/internal_unmqr.o src/internal/internal_unmlq.o src/internal/internal_util.o src/internal/internal_hegst.o src/internal/internal_gescale.o src/internal/internal_tzscale.o src/internal/internal_tzadd.o src/cuda/device_geadd.o src/cuda/device_gecopy.o src/cuda/device_genorm.o src/cuda/device_geset.o src/cuda/device_henorm.o src/cuda/device_synorm.o src/cuda/device_transpose.o src/cuda/device_trnorm.o src/cuda/device_tzcopy.o src/cuda/device_gescale.o src/cuda/device_tzscale.o src/cuda/device_tzadd.o src/bdsqr.o src/colNorms.o src/copy.o src/gbmm.o src/gbsv.o src/gbtrf.o src/gbtrs.o src/ge2tb.o src/gels.o src/gemm.o src/gemmA.o src/geqrf.o src/gelqf.o src/gesv.o src/gesv_nopiv.o src/gesvd.o src/gesvMixed.o src/getrf.o src/getrf_nopiv.o src/getri.o src/getriOOP.o src/getrs.o src/getrs_nopiv.o src/hb2st.o src/he2hb.o src/unmtr_he2hb.o src/heev.o src/hemm.o src/hemmA.o src/hbmm.o src/her2k.o src/herk.o src/hesv.o src/hetrf.o src/hetrs.o src/hegv.o src/norm.o src/pbsv.o src/pbtrf.o src/pbtrs.o src/print.o src/posv.o src/posvMixed.o src/potrf.o src/potri.o src/potrs.o src/set.o src/sterf.o src/steqr2.o src/symm.o src/syr2k.o src/syrk.o src/tb2bd.o src/tbsm.o src/tbsmPivots.o src/trmm.o src/trsm.o src/trtri.o src/trtrm.o src/unmqr.o src/unmlq.o src/hegst.o src/scale.o src/add.o src/ssteqr2.o src/dsteqr2.o src/csteqr2.o src/zsteqr2.o src/c_api/util.o src/c_api/matrix.o src/c_api/wrappers.o src/c_api/wrappers_precisions.o \
        -lblaspp -llapackpp -lgfortran -lnvToolsExt -lmkl_gf_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl -lcublas -lcudart \
        -shared  -o lib/libslate.so
mpicxx -L./lib -fPIC -fopenmp -L./blaspp/lib -L./lapackpp/lib scalapack_api/scalapack_gels.o scalapack_api/scalapack_gemm.o scalapack_api/scalapack_getrf.o scalapack_api/scalapack_getrs.o scalapack_api/scalapack_gesv.o scalapack_api/scalapack_hemm.o scalapack_api/scalapack_her2k.o scalapack_api/scalapack_herk.o scalapack_api/scalapack_lanhe.o scalapack_api/scalapack_lange.o scalapack_api/scalapack_lansy.o scalapack_api/scalapack_lantr.o scalapack_api/scalapack_potrf.o scalapack_api/scalapack_potri.o scalapack_api/scalapack_posv.o scalapack_api/scalapack_symm.o scalapack_api/scalapack_syr2k.o scalapack_api/scalapack_syrk.o scalapack_api/scalapack_trmm.o scalapack_api/scalapack_trsm.o \
        -lslate -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lblaspp -llapackpp -lgfortran -lnvToolsExt -lmkl_gf_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl -lcublas -lcudart -shared  -o lib/libslate_scalapack_api.so
mpicxx -L./lib -fPIC -fopenmp -L./blaspp/lib -L./lapackpp/lib lapack_api/lapack_gels.o lapack_api/lapack_gemm.o lapack_api/lapack_gesv.o lapack_api/lapack_gesvMixed.o lapack_api/lapack_getrf.o lapack_api/lapack_getri.o lapack_api/lapack_getrs.o lapack_api/lapack_hemm.o lapack_api/lapack_her2k.o lapack_api/lapack_herk.o lapack_api/lapack_lange.o lapack_api/lapack_lanhe.o lapack_api/lapack_lansy.o lapack_api/lapack_lantr.o lapack_api/lapack_posv.o lapack_api/lapack_potrf.o lapack_api/lapack_potri.o lapack_api/lapack_symm.o lapack_api/lapack_syr2k.o lapack_api/lapack_syrk.o lapack_api/lapack_trmm.o lapack_api/lapack_trsm.o \
        -lslate -lblaspp -llapackpp -lgfortran -lnvToolsExt -lmkl_gf_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -ldl -lcublas -lcudart -shared  -o lib/libslate_lapack_api.so
kadir@leconte 09:34:53 Feb 09 Wed ~/t012/slate-dev $ make generate  VERBOSE=1
make: Nothing to be done for `generate'.
kadir@leconte 09:37:41 Feb 09 Wed ~/t012/slate-dev $ echo " " >> include/slate/c_api/types.h
kadir@leconte 09:37:44 Feb 09 Wed ~/t012/slate-dev $ make generate  VERBOSE=1
python tools/c_api/generate_util.py include/slate/c_api/types.h include/slate/c_api/util.hh
```

### 2022-02-14 Monday

Fix python in CMakeLists.txt
