### Installing SLATE and its tutorial on Leconte

Clone the `t012` repo inside the `$HOME` folder.

The following steps will install SLATE and its tutorial on Leconte:

```
cd $HOME/t012/cmake-capi
source ../install-scripts/slate-leconte-cmake.sh
```

At the end of a successful installation, the following output
should be seen:

```
rank 2: test_matrix_r32
rank 1: test_matrix_r32
rank 3: test_matrix_r32
rank 0: test_matrix_r32
rank 2: test_matrix_r64
rank 2: test_matrix_c32
rank 2: test_matrix_c64
rank 3: test_matrix_r64
rank 3: test_matrix_c32
rank 3: test_matrix_c64
rank 1: test_matrix_r64
rank 0: test_matrix_r64
rank 1: test_matrix_c32
rank 0: test_matrix_c32
rank 1: test_matrix_c64
rank 0: test_matrix_c64
```

