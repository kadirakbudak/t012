## Usage:
## cd slate-dev
## source ../install-scripts/slate-leconte-v2.sh
echo "usage:"
echo "    source ../install-scripts/slate-leconte-v2.sh"
echo

module load gcc/7.3.0
module load cuda/11.2.0
module load intel-mkl
module load intel-mpi

git submodule update --init --recursive

cat > make.inc << END
CXX       = mpicxx
FC        = mpif90
CXXFLAGS  = -g -Werror -DSLATE_NO_HIP
blas      = mkl
cuda_arch = volta
# openmp=1 by default
hip       = 0
prefix    = install
c_api     = 1

# print time in fortran code
LIBS += -lgfortran

# for tracing
LIBS += -lnvToolsExt
NVCCFLAGS = -lineinfo
END

echo "========================================"
make distclean
echo "========================================"
ls -R
echo "========================================"
time make -j20
echo "========================================"
make -j20 install
echo "========================================"
ldd test/tester

## Comment out to run tests
# cd test
# ./run_tests.py --ref n --xml ../report.xml
