## Usage:
## cd cmake-capi
## source ../install-scripts/slate-leconte-cmake.sh
echo "usage:"
echo "    source ../install-scripts/slate-leconte-cmake.sh"
echo

install_slate=1
install_tutorial=1
if [ ${install_slate} -eq 1 ]; then
    git clean -f  ## remove untracked files

    module purge
    module load gcc/8.3.0
    module load cuda/11.6.0
    module load intel-oneapi-mkl/2022.0.2
    module load intel-oneapi-mpi/2021.1.1
    module load cmake/3.19.4
    module load python
    export LIBRARY_PATH=$CUDADIR/lib64:$LIBRARY_PATH
    export CPATH=$CUDADIR/include:$CPATH

    git submodule update --init --recursive

    if [ -d build ]; then
        rm -rf ./build
    fi
    mkdir build \
        && cd build \
        && cmake .. -DCMAKE_INSTALL_PREFIX=./install \
            -DCMAKE_CUDA_ARCHITECTURES=70 \
            -Dc_api=yes \
        && make -j 20 lib VERBOSE=1 \
        && make -j 20 install VERBOSE=1 \
        && ./test/tester gemm

    if [ $? -eq 0 ]; then
        echo "SLATE is successfully installed"
    else
        echo "Installation of SLATE failed"
        install_tutorial=0
        cd ..
    fi
    cd .. && cd ..
fi
if [ ${install_tutorial} -eq 1 ]; then
    cd slate-tutorial \
        && cd c_api \
        && make \
            slate_dir=${HOME}/t012/cmake-capi/build/install \
            scalapack_libs="-L${MKLROOT}/lib/intel64 -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64" \
            lib_suffix=64 \
        && mpirun -np 4 ./slate00_matrix
fi
