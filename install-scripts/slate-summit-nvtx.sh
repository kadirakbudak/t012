# cd slate-dev
## https://bitbucket.org/icl/slate/wiki/machines/summit.md

## On Summit, SLATE is compiled using the GCC compiler suite. 
## Load up modules to compile SLATE
module unload xl
module load gcc/9.1.0
module load essl
module load cuda/11
module load spectrum-mpi
module load netlib-lapack
module load netlib-scalapack
module load python

## The modules do not set the necessary LIBRARY_PATH. Update the LIBRARY_PATH so that needed libraries are found during compilation. 
export LIBRARY_PATH=$OLCF_ESSL_ROOT/lib64:$LIBRARY_PATH
export LIBRARY_PATH=$CUDA_DIR/lib64:$LIBRARY_PATH

git submodule update --init

export color=no

cat > make.inc << END
CXX=mpicxx
FC=mpif90
blas=essl
cuda_arch=volta
END

echo "========================================"
make distclean
echo "========================================"
ls -R
echo "========================================"
nice make -j 4
echo "========================================"
ldd test/tester
